import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(
          "Home Page",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.purple,
      ),
      body: Column(
        children: [
          Text("Hello Word"),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextFormField(
              decoration: InputDecoration(
                hintText: "Masukan Nama",
                focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(
                      width: 1,color: Colors.purpleAccent,
                    )
              ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(
                    width: 1,color: Colors.purpleAccent,
                  )
                )
              ),
            ),
          ),
          ElevatedButton(onPressed: () {},
              child: Text("Simpan"),
            style: ElevatedButton.styleFrom(
              backgroundColor: Colors.white60
            ),
          ),
        ],
      ),
    );
  }
}
